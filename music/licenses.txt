Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Music for Wasted
Upstream-Contact: Franzo <franzo@dismail.de>
Comment: This file should hopefully be complete.

Files: *.music
Copyright: NA
License: NA
Comment: Trivial info files

Files: kart_grand_prix.ogg
Copyright: Weirwood
License: CC-BY-SA v3.0

Files: lose_theme.ogg
Copyright: Weirwood
License: CC-BY-SA v3.0

Files: menutheme.ogg
Copyright: REDCVT (edited by Franzpow)
License: CC-BY-SA 4.0

Files: race_summary.ogg
Copyright: Dundersylt
License: CC-BY-SA 3.0+

Files: win_theme.ogg
Copyright: Claude Werner (bollen)
License: CC-BY-SA 3.0

Files: race_win_theme.ogg
Copyright: Weirwood
License: CC-BY-SA 3.0
